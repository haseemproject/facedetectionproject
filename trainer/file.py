#!/usr/bin/python

import numpy as np
import cv2

detector= cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
cam = cv2.VideoCapture(0)




sampleNum=0
while(True):
    ret, img = cam.read()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = detector.detectMultiScale(gray, 1.3, 5)
    for (x,y,w,h) in faces:
        cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
	#cv2.circle(img, (x, y), 20, (0, 0, 255), -1)
        sampleNum=sampleNum+1
        cv2.imwrite("/home/haseem/detection/dataset/."+'.'+ str(sampleNum) + ".jpg", gray[y:y+h,x:x+w])

    cv2.imshow('frame',img)

    if cv2.waitKey(500) & 0xFF == ord('q'):
        break
    # break if the sample number is morethan 20
    elif sampleNum>20:
        break
cam.release()
cv2.destroyAllWindows()
