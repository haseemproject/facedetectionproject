#!/usr/bin/python
import cv2
import numpy as np

recognizer = cv2.face_LBPHFaceRecognizer.create()
recognizer.read('/mnt/facedetectionproject/samples/trainingdata.yml')
cascadePath = "/mnt/facedetectionproject/recognizer/haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascadePath);


cam = cv2.VideoCapture(0)
#font = (cv2.FONT_HERSHEY_COMPLEX_SMALL, 5, 1, 0, 4)
#print font
while True:
    ret, im =cam.read()
    gray=cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)
    faces=faceCascade.detectMultiScale(gray, 1.2,5)
    for(x,y,w,h) in faces:
        cv2.rectangle(im,(x,y),(x+w,y+h),(225,0,0),2)
        Id, conf = recognizer.predict(gray[y:y+h,x:x+w])
        if(conf<50):
            if(Id==1):
                Id="Haseem"
            elif(Id==2):
                Id="AR"
	    elif(Id==3):
		Id="JH"
	    elif(Id==4):
		Id="Mirza"
	    elif(Id==5):
		Id="Ibtehaj"
	    elif(Id==6):
	        Id="aamir"
	    elif(Id==7):
	        Id="osama"
	    elif(Id==8):
		Id="Khadim"
            elif(Id==9):
	        Id="munim"
        else:
            Id="Unknown"
        cv2.putText(im,str(Id), (x,y+h), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255),2,lineType=cv2.LINE_AA)
    cv2.imshow('im',im) 
    if(cv2.waitKey(1)==ord('q')):
        break;
cam.release()
cv2.destroyAllWindows()
